package in.sunilpaulmathew.izzyondroid.utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;

/*
 * Created by sunilpaulmathew <sunil.kde@gmail.com> on August 19, 2021
 */
public class RecyclerViewItems implements Serializable {

    private final JSONArray mPhoneScreenshots;
    private final JSONObject mRawData;
    private final long mAdded, mLastUpdated;
    private final String mAuthorName, mAuthorEmail, mAuthorWeb, mDescription, mDonation, mLicence, mPackageName,
            mSource, mSummary, mTitle, mUrl, mVersion, mWhatsNew;

    public RecyclerViewItems(JSONObject rawData, String authorName, String authoremail, String authorweb,
                             String description, String donation, String licence, long added, long lastUpdated,
                             String packageName, String source, String summary, String title, String url, String version,
                             String whatsNew, JSONArray phoneScreenshots) {
        this.mRawData = rawData;
        this.mAuthorName = authorName;
        this.mAuthorEmail = authoremail;
        this.mAuthorWeb = authorweb;
        this.mDescription = description;
        this.mDonation = donation;
        this.mLicence = licence;
        this.mAdded = added;
        this.mLastUpdated = lastUpdated;
        this.mPackageName = packageName;
        this.mSource = source;
        this.mSummary = summary;
        this.mTitle = title;
        this.mUrl = url;
        this.mVersion = version;
        this.mWhatsNew = whatsNew;
        this.mPhoneScreenshots = phoneScreenshots;
    }

    public JSONArray getPhoneScreenshots() {
        return mPhoneScreenshots;
    }

    public JSONObject getRawData() {
        return mRawData;
    }

    public long getAddedDate() {
        return mAdded;
    }

    public long getLastUpdated() {
        return mLastUpdated;
    }

    public String getAuthorName() {
        return mAuthorName;
    }

    public String getAuthorEmail() {
        return mAuthorEmail;
    }

    public String getAuthorWeb() {
        return mAuthorWeb;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getDonationLink() {
        return mDonation;
    }

    public String getLicence() {
        return mLicence;
    }

    public String getPackageName() {
        return mPackageName;
    }

    public String getSource() {
        return mSource;
    }

    public String getSummary() {
        return mSummary;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getImageUrl() {
        return mUrl;
    }

    public String getVersionCode() {
        return mVersion;
    }

    public String getChangeLogs() {
        return mWhatsNew;
    }

}