package in.sunilpaulmathew.izzyondroid.adapters;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.card.MaterialCardView;
import com.google.android.material.textview.MaterialTextView;
import com.squareup.picasso.Picasso;

import java.util.List;

import in.sunilpaulmathew.izzyondroid.R;
import in.sunilpaulmathew.izzyondroid.utils.Common;
import in.sunilpaulmathew.izzyondroid.utils.RecyclerViewItems;
import in.sunilpaulmathew.sCommon.Utils.sAPKUtils;
import in.sunilpaulmathew.sCommon.Utils.sPackageUtils;
import in.sunilpaulmathew.sCommon.Utils.sUtils;

/*
 * Created by sunilpaulmathew <sunil.kde@gmail.com> on August 19, 2021
 */
public class LatestAppsAdapter extends RecyclerView.Adapter<LatestAppsAdapter.ViewHolder> {

    private static List<RecyclerViewItems> data;
    public LatestAppsAdapter(List<RecyclerViewItems> data){
        LatestAppsAdapter.data = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rowItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_latest_apps, parent, false);
        return new ViewHolder(rowItem);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try {
            holder.mName.setText(data.get(position).getTitle());
            holder.mSummary.setText(data.get(position).getSummary());
            Picasso.get().load(data.get(position).getImageUrl()).placeholder(R.drawable.ic_android).into(holder.mIcon);
            if (!sUtils.isDarkTheme(holder.mMainCard.getContext())) {
                holder.mMainCard.setCardBackgroundColor(Color.LTGRAY);
            }
            if (sPackageUtils.isPackageInstalled(data.get(position).getPackageName(), holder.mMenuIcon.getContext()) && sAPKUtils.getVersionCode(
                    sPackageUtils.getSourceDir(data.get(position).getPackageName(), holder.mMenuIcon.getContext()), holder.mMenuIcon.getContext()) < Integer.parseInt(data.get(position).getVersionCode())) {
                holder.mMenuIcon.setVisibility(View.VISIBLE);
            } else {
                holder.mMenuIcon.setVisibility(View.GONE);
            }
        } catch (IndexOutOfBoundsException | NullPointerException ignored) {}
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final AppCompatImageButton mMenuIcon;
        private final AppCompatImageView mIcon;
        private final MaterialTextView mName, mSummary;
        private final MaterialCardView mMainCard;

        public ViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            this.mMenuIcon = view.findViewById(R.id.menu_button);
            this.mIcon = view.findViewById(R.id.icon);
            this.mMainCard = view.findViewById(R.id.card_main);
            this.mSummary = view.findViewById(R.id.short_summary);
            this.mName = view.findViewById(R.id.name);
        }

        @Override
        public void onClick(View view) {
            Common.launchPackageView(data.get(getAdapterPosition()), view.getContext());
        }
    }

}